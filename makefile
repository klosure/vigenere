CXX=c++
RM=rm -f
CXXFLAGS=--std=c++14 -pedantic -Wall -Wextra
LDFLAGS = -I include
LDFLAGS += -I tests
LDFLAGS += -I /usr/local/include
LDLIBS=-L/usr/local/lib -lpthread

.POSIX: all debug opt

all: vigenere

debug: CXXFLAGS += -O0 -ggdb -save-temps -fsanitize=undefined -fsanitize=address -fno-omit-frame-pointer -fno-sanitize-recover
debug: LDFLAGS += -lasan -lubsan
debug: clean vigenere

opt: CXXFLAGS += --pipe -O2 -fpie -march=native -fstack-protector-strong
opt: LDFLAGS += -pie -Wl,-z,relro -Wl,-z,now
opt: clean vigenere

vigenere: vigenere.o runner.o
	$(CXX) $(LDFLAGS) -o vigenere vigenere.o runner.o $(LDLIBS)

vigenere.o: vigenere.cpp
	$(CXX) $(LDFLAGS) $(CXXFLAGS) -c vigenere.cpp

runner.o: runner.cpp
	$(CXX) $(LDFLAGS) $(CXXFLAGS) -c runner.cpp

clean:
	$(RM) *.o *.s *.ii

distclean: clean
	$(RM) *.key *.tar.gz *~ vigenere test_vigenere

#test: test.cpp
#> run tests

dist: distclean
	tar -czvf ../vigenere-source.tar.gz ./
	mv ../vigenere-source.tar.gz ./
