// Vigen�re Cipher Implementation
//
// Runner - Responsible for executing the program
//
// https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher
// Author: klosure
// License: BSD 3-clause

#include "runner.hpp"

// --- Constructors ---

Runner::Runner(int &argc, char *argv[]) {
  p.argc = argc;
  p.argv = argv;
}
Runner::~Runner(){}

// --- Member Functions ---

inline void Runner::print_version() const {
  std::cout << "vigenere version: " << VERSION << "\n";  
}


inline void Runner::error() {
  std::cout << "Sorry, I don't understand that input." << std::endl;
}


inline void Runner::lower(std::string &e_or_d) {
  std::for_each(e_or_d.begin(), e_or_d.end(), [](char & c) {
      c = ::tolower(c);
  });
}


inline void Runner::flush() {
  std::cin.clear();
  std::cin.ignore();
}


// Used to display the interactive portion of the application.
void Runner::shell() {
  std::string enc_or_dec;
  std::cout << "Would you like to encrypt or decrypt a message (e/d): ";
  std::cin >> enc_or_dec;
  flush();
  lower(enc_or_dec);
  // check for invalid input
  if (!(enc_or_dec == "e" || enc_or_dec == "d") || enc_or_dec == "encrypt" || enc_or_dec == "decrypt") {
    // we received bad input!
    error();
    throw "bad user input";
  }
  else if (enc_or_dec == "e" || enc_or_dec == "encrypt") {
    // the user wants to encrypt a file
    p.enc = true;
    std::cout << "What message would you like to encrypt? : ";
    std::string in;
    std::getline(std::cin, in);
    p.message = in;
  }
  else {
    // the user wants to decrypt a file
    std::cout << "What file would you like to decrypt? : ";
    std::cin >> p.input_filename;
  }
  std::cout << "What would you like to name the output file? : ";
  std::cin >> p.output_filename;
  flush();
  if (p.enc) {
    std::cout << "How many alphabets to create? (1 alphabet per character recommended) : ";
    int temp = 0;
    std::cin >> temp;
    if (temp < 1) { throw "bad user input"; }
    else { p.noa = temp; }
    flush();
  }
}

// lets look at what the user input
bool Runner::parse_args() {

  // if no arguments passed in start interactive mode (i.e. shell mode, repl mode, whatevs)
  // used for entering raw text, file input is done through cli args
  if (p.argc < 2) {
    // loop over user input
    try {
      shell();
    }
    catch (...) {
      std::cout << "shell input exception caught";
      error();
      return false;
    }
  }
  else {
    // if flags are used, we expect at least 8 pieces of info
    p.cli_exec = true;
    CLI::App app{ "Takes a string of plaintext, encrypts it, then returns a ciphertext." };
    app.ignore_case();
    
    app.add_flag("-e,--encrypt", "encrypt a file");
    app.add_flag("-d,--decrypt", "decrypt a file");	
    app.add_flag("-v,--version", "display the version");
	  
    app.add_option("-i,--input", p.input_filename, "file to read in");
    app.add_option("-o,--output", p.output_filename, "file to output results");
    app.add_option("-n,--numalphabets", p.noa, "number of alphabets to use");
    app.add_option("-k,--keyfile", p.key_filename, "key file to use");
    app.add_option("-t,--threads", p.num_threads, "number of threads to spawn");

    CLI11_PARSE(app, p.argc, p.argv);

    if (app.count("-v")) {
      p.ver = true;
      return true;
    } 
    else if (app.count("-e")) {
      p.enc = true;
    }
    else if (!app.count("-d")) {
      // encrypt by default, even without -e flag
      p.enc = true;
    }
    if (!app.count("-t")) {
      p.num_threads = std::thread::hardware_concurrency();
    }
    else {
      if (p.num_threads < 1 || p.num_threads > 999) { throw "bad user input"; }
    }
    if (app.count("-n")) {
      if (p.noa < 1) { throw "bad user input"; }
    }
    if (!app.count("-i") || !app.count("-o")) {
      throw "missing required argument: -i or -o";
    }
  }
  return true;
}

// contains the main logic of the application (keeps main() smaller)
bool Runner::run() {
  bool success = false;
  try {
    success = parse_args();
  }
  catch (...) {
    std::cout << "parsing exception caught";
    error();
  }
  if (success) {
    if (p.ver) {
      this->print_version();
      return true;
    }
    // cli encryption
    else if (p.enc && p.cli_exec) {
      std::unique_ptr<Vigenere> sp_vigenere_obj = std::make_unique<Vigenere>(p.noa, p.key_filename, p.output_filename, p.input_filename, p.num_threads);
      sp_vigenere_obj->start_workers(encrypt_work);
      sp_vigenere_obj->write_output(p.output_filename);
    }
    // interactive encryption
    else if (p.enc && !p.cli_exec) {
      std::unique_ptr<Vigenere> sp_vigenere_obj = std::make_unique<Vigenere>(p.noa, p.output_filename, p.message, p.num_threads);
      sp_vigenere_obj->start_workers(encrypt_work);
      sp_vigenere_obj->write_output(p.output_filename);
    }
    // cli decryption OR interactive decryption
    else if ((!p.enc && p.cli_exec) || (!p.enc && !p.cli_exec)) {
      std::unique_ptr<Vigenere> sp_vigenere_obj;
      try {
	sp_vigenere_obj = std::make_unique<Vigenere>(p.input_filename, p.output_filename, p.key_filename, p.num_threads);
      }
      catch (...) {
	std::cout << "construction exception caught";
	return false;
      }
      sp_vigenere_obj->start_workers(decrypt_work);
      sp_vigenere_obj->write_output(p.output_filename);
    }
    else {
      // you done goof'd
      throw "wat";
    }
    return true;
  }
  else {
    return false;
  }
}

// --- Non-member functions ---

int main(int argc, char *argv[]) {
  srand(time(NULL));
  // seed the initial random value
  
  std::unique_ptr<Runner> sp_runner = std::make_unique<Runner>(argc, argv);
  bool ranOk = sp_runner->run();

  if (ranOk) {
    return 0;
  }
  else {
    return 1;
  }
}
