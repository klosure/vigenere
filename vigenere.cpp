// Vigen�re Cipher Implementation
//
// Vigenere - Handles the encryption of strings
//
// https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher
// Author: klosure
// License: BSD 3-clause


#include "vigenere.hpp"

typedef unsigned int u_int;

// --- Constructors ---
	
// encrypt interactively (new generic key)
Vigenere::Vigenere(u_int rows, std::string output_filename, std::string message, u_shrt num_threads) : num_rows(rows) {
  std::string key_filename = "vc.key";
  divide_text(message, num_threads);
  if (!key_exists(key_filename)) {
    generate_rows();
    generate_key_order();
    write_key(key_filename);
  }
  else {
    open_key(key_filename);
  }
}

// encrypt via cli (new specific key name)
Vigenere::Vigenere(u_int rows, std::string key_filename, std::string output_filename, std::string input_filename, u_shrt num_threads) : num_rows(rows) {
  std::string message = read_input_text(input_filename);
  divide_text(message, num_threads);
  // if that keyfile name exists, skip keygen
  if (!key_exists(key_filename)) {
    generate_rows();
    generate_key_order();
    write_key(key_filename);
  }
  else {
    open_key(key_filename);
  }
}

// decrypt with existing key
Vigenere::Vigenere(std::string input_filename, std::string output_filename, std::string key_filename, u_shrt num_threads) {
  // check that both the key and input files exist
  if (key_exists(key_filename) && key_exists(input_filename)) {
    open_key(key_filename);
    std::string message = read_input_text(input_filename);
    divide_text(message, num_threads);
  }
  else {
    throw "failed construction, missing files?";
  }
}

Vigenere::~Vigenere(){}


// --- Static Functions ---


// used by a thread
void encrypt_work(const std::string &plaintext, Chunk &c, const std::vector<std::vector<char>> &final_matrix, const std::vector<u_int> &key_order) {
  u_int key_start = c.get_sa();
  if (key_start >= final_matrix.size()) {
    // we have more Chunks than alphabets..
    key_start = 0;
  }
  std::ostringstream ciphertext;
  // for each character, check "key row num", replace character
  std::string::const_iterator it;
  // for each character in string ; encrypt
  for (it = plaintext.begin(); it != plaintext.end(); ++it) {
    const char plaintext_char = plaintext.at(it - plaintext.begin());
    // verbose version
    //const char index_of_ptc = plaintext_char - ASCII_VAL;
    //const char encrypted_char = final_matrix[key_order[key_start]][index_of_ptc];
    //ciphertext << encrypted_char;

    if (plaintext_char == '\n') {
      // skip conversion for newline char and just add it in
      ciphertext << '\n';
    }
    else {
      // short version
      ciphertext << final_matrix[key_order[key_start]][plaintext_char - ASCII_VAL];
    }
    // keep track of our key..
    key_start = (key_start + 1) != final_matrix.size() ? (key_start + 1) : 0;
  }
  c.result.set_value(ciphertext.str());
}


// used by a thread
void decrypt_work(const std::string &ciphertext, Chunk &c, const std::vector<std::vector<char>> &final_matrix, const std::vector<u_int> &key_order) {
  u_int key_start = c.get_sa();
  if (key_start >= final_matrix.size()) {
    // we have more Chunks than alphabets. Start at beginning.
    key_start = 0;
  }
  std::ostringstream plaintext;
  // for each character, check "key row num", replace character
  std::string::const_iterator it;
  for (it = ciphertext.begin(); it != ciphertext.end(); ++it)
    {
      if (ciphertext.at(it - ciphertext.begin()) == '\n') {
	// skip conversion for newline char and just add it in
	plaintext << '\n';
      }
      else {
	const std::vector<char> &row = final_matrix[key_order[key_start]];
	const u_int pos = distance(row.begin(), find(row.begin(), row.end(), *it));
	if (pos >= row.size()) {
	  std::cout << "row not found" << std::endl;
	}
	else {
	  const char decrypted_char = pos + ASCII_VAL ;
	  plaintext << decrypted_char;
	}
      }
      key_start = (key_start + 1) != final_matrix.size() ? (key_start + 1) : 0;
    }
  c.result.set_value(plaintext.str());
}


// --- Member Functions ---


// takes a string of plaintext, encrypts and returns ciphertext
// calls either encrypt_work or decrypt_work, depending on which function was passed in
void Vigenere::start_workers(std::function<void (const std::string&, Chunk&, const std::vector<std::vector<char>>&, const std::vector<u_int>&)> work) const {
  std::vector<std::thread> vt;
  // for each character, check "key row num", replace character
  // loop over each chunk of plaintext; we want to work on a chunk of plaintext
  for (const auto &c : split_pt) {
    // send the copy to a thread to process.
    vt.push_back(std::thread([&]() {
	  work(c->get_text(), *c, final_matrix, key_order);
	}));
  }
  //wait for all threads to finish work
  for (auto &t : vt) {
    t.join();
  }
}


// used to access a preexisting keyfile on disk
bool Vigenere::open_key(std::string key_file) {
  // read in key file
  std::vector<u_int> opened_key_order;
  std::vector<char> matrix_line;
  std::vector<std::vector<char>> opened_final_matrix;
  std::ifstream file(key_file);
  std::string this_line;
  u_int rows = 0;

  bool key_order_line = true;
  std::string build = "";
  while (getline(file, this_line)){
    std::istringstream iss(this_line);
    if (key_order_line) {
      // fill key_order
      for (auto &c : iss.str()) {
	if (c == ',') {
	  opened_key_order.push_back(std::stoi(build));
	  build = "";
	}
	else {
	  build += c;
	}
      }
      key_order_line = false;
    }
    else {
      // fill final_matrix
      for (auto &c : iss.str()) {
	matrix_line.push_back(char(c));
      }
      opened_final_matrix.push_back(matrix_line);
      matrix_line.clear();
      ++rows;
    }
  }
  final_matrix = opened_final_matrix;
  key_order = opened_key_order;
  num_rows = rows;
  return true;
}


// write out a newly generated keyfile to disk
bool Vigenere::write_key(std::string &key_filename) const {
  std::ofstream file;
  std::ostringstream oss;
  file.open(key_filename);
  // convert vec to string (row 1 is key order)
  if (!key_order.empty()) {
    for (auto &c : key_order) {
      oss << c;
      oss << ",";
    }
    oss << "\n";
    file << oss.str();
  }
  if (!final_matrix.empty()) {
    std::vector<std::vector<char>>::const_iterator v_it;
    for (v_it = final_matrix.begin(); v_it != final_matrix.end(); ++v_it) {
      for (auto &x : *v_it) {
	file << x;
      }
      file << "\n";
    }
  }
  // converts matrix to string (rows 2 -> n are the key_matrix)
  file.close();
  return true;
}


// write the resulting ciphertext out to a file
void Vigenere::write_output(std::string &result_file) const {
  std::ofstream file;
  std::ostringstream oss;
  file.open(result_file);
  for (auto &c : split_pt) {
    oss << c->result.get_future().get();
  }
  file << oss.str();
  file.close();
}


// read in text from a file on disk
std::string Vigenere::read_input_text(std::string &path) const {
  std::ifstream src(path);
  if (!src) {
    perror("Error opening file ");
    system("pause");
  }
  std::string line;
  std::string ret_str;
  while (getline(src, line)) {
    // use line
    ret_str += line;
    ret_str += "\n";
  }
  return ret_str;
}


// divide up num of rows
// for each row to make
// copy a_thru_z, and swap random positions
// lock / insert row into alpha_rows / release lock?
// candidate for threading - CFT
void Vigenere::generate_rows() {
  // reference alphabet
  for (char i = ' '; i < '~'; i++) {
    a_thru_z.push_back(i);
  }
  // for each row..
  for (u_int i = 0; i < num_rows; i++) {
    // a_thru_z is prepopulated
    // so copy it
    std::vector<char> current_copy = a_thru_z;

    // iterate through our initialized row and randomly swap
    for (char i = ' '; i < '~'; i++) {
      int coin_flip = (rand() % 2) + 1;
      if (coin_flip > 1) {
	int letter_picked = (rand() % 93) + 1;
	iter_swap(current_copy.begin() + (i - 32), current_copy.begin() + letter_picked);
      }
    }
    // save it into the final matrix
    final_matrix.push_back(current_copy);
  }
}


// for total number of rows, pick a random row.
void Vigenere::generate_key_order() {
  for (u_int i = 0; i < num_rows; i++) {
    int rand_row = (rand() % num_rows);
    key_order.push_back(u_int(rand_row));
  }
}

// fast way to check for file existence
inline bool Vigenere::key_exists(const std::string &name) const {
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}


// divide up text into equal or mostly equal 'chunks' for thread consumption
// fills a vector with Chunk objects
// and returns the size of each of those chunks (minus the last chunk, which may or may not be the same size)
void Vigenere::divide_text(std::string &pt, u_shrt num_threads) {
  // how many threads can run?
  const u_int text_len = pt.length();
  u_int chnk_size;
  u_int remainder = 0;
  // even or odd?
  if(text_len % 2 == 0) {
    // even
    while (text_len < num_threads) {
      // not enough work for specified number of threads
      --num_threads;
    }
    chnk_size = text_len / num_threads;
  }
  else {
    // odd
    while (text_len < num_threads) {
      // not enough work for specified number of threads
      --num_threads;
    }
    chnk_size = (text_len / num_threads);
    remainder = text_len - (chnk_size * num_threads);
  }
  // i counts chars
  u_int i = 0;
  // j counts chunks
  u_int j = 0;
  std::string chunk_build;
  u_int left = remainder;
  // split up message into Chunks
  for (auto& c : pt) {
    if (j == u_int((num_threads - 1))) {
      // on last chunk
      chunk_build += c;
      if (((i + 1) == chnk_size) && (left != 0)) {
	// hit max chunk size, filling in the last leftover odd chars
	--left;
	continue;
      }
      if (((i + 1) == chnk_size) && left == 0) {
	std::unique_ptr<Chunk> sp_chnk = std::make_unique<Chunk>(chunk_build, (&c - &pt[0]) - ((chnk_size+remainder) - 1));
	split_pt.push_back(move(sp_chnk));
	break;
      }
      ++i;
    }
    else if (i < chnk_size) {
      ++i;
      // not done building chunk
      chunk_build += c;
      if (i == chnk_size && j != u_int((num_threads - 1))) {
	// hit max chunk size
	// &c - &pt[0] is the index of c
	std::unique_ptr<Chunk> sp_chnk = std::make_unique<Chunk>(chunk_build, (&c - &pt[0]) - (chnk_size - 1));
	split_pt.push_back(move(sp_chnk));
	++j;
	// reset i and chunk_build since we are starting a new chunk
	i = 0;
	chunk_build = "";
	continue;
      }
    }
  }
}
