#ifndef MY_VC
#define MY_VC

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <thread>
#include <iterator>
#include <random>
#include <algorithm>
#include <cstdlib>     // srand, rand
#include <time.h>      // gets the time 
#include <sys/stat.h>  // used to check file existence
#include <functional>
#include "chunk.hpp"


typedef unsigned short u_shrt;

// used to convert ascii into index (a=0, b=1, c=2)
constexpr char ASCII_VAL = char(32);

void encrypt_work(const std::string &plaintext, Chunk &c, const std::vector<std::vector<char>> &final_matrix, const std::vector<u_int> &key_order);
void decrypt_work(const std::string &ciphertext, Chunk &c, const std::vector<std::vector<char>> &final_matrix, const std::vector<u_int> &key_order);

// Vigen�re Cipher Class declarations
class Vigenere {
public:
  // encrypt using generic key name
  Vigenere(u_int rows, std::string output_filename, std::string message, u_shrt num_threads);
  // encrypt (no existing key) specify key name
  Vigenere(u_int rows, std::string key_filename, std::string output_filename, std::string message, u_shrt num_threads);
  // decrypt with existing key
  Vigenere(std::string input_filename, std::string output_filename, std::string key_filename, u_shrt num_threads);
  Vigenere(Vigenere &vc_obj) = delete;
  Vigenere() = delete;
  ~Vigenere();


  // takes a string of plaintext, encrypts and returns ciphertext
  void start_workers(std::function<void(const std::string&, Chunk&, const std::vector<std::vector<char>>&, const std::vector<u_int>&)> work) const;

  // write the resulting ciphertext out to a file
  void write_output(std::string &result_file) const;

  // subsets of the initial plaintext for thread consumption
  std::vector<std::unique_ptr<Chunk>> split_pt;

  u_int num_rows = 1;
  // Tabula recta
  std::vector<std::vector<char>> final_matrix;
  std::vector<u_int> key_order;
  std::vector<char> a_thru_z;


private:

  // opens a preexisting key
  bool open_key(std::string key_file);

  // writes out a vc.key file
  bool write_key(std::string &key_filename) const;

  // reads in a text file to process
  std::string read_input_text(std::string &path) const;

  // divide up num of rows
  // for each row to make
  // copy a_thru_z, and swap random positions
  void generate_rows();

  // for total number of rows, pick a random row.
  void generate_key_order();

  bool key_exists(const std::string &key_filename) const;

  // divide up text into equal or mostly equal 'chunks' for thread consumption
  // fills a vector with Chunk objects
  // and returns the size of each of those chunks (minus the last chunk, which may or may not be the same size)
  void divide_text(std::string &pt, u_shrt num_threads);
};

#endif
