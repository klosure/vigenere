#ifndef MY_CHUNK
#define MY_CHUNK

#include <future>
#include <string>

typedef unsigned int u_int;


class Chunk {

public:
  Chunk() = delete;
  Chunk(std::string plain, u_int start_alpha) : text(plain), sa(start_alpha) {}
  Chunk(Chunk&) = default;
  ~Chunk() {}

  const std::string& get_text() { return text; }
  u_int get_sa() { return sa; }

  // result of a threads work on our plaintext/ciphertext
  std::promise<std::string> result;

private:
  // our chunk of plaintext to be worked on
  std::string text;
  // where does our alphabet start (e.g. the index of the first char in plain relative to the full message)?
  u_int sa;
};

#endif
