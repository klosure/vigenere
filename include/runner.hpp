#ifndef MY_RUNNER
#define MY_RUNNER

#include "CLI11.hpp"
#include "vigenere.hpp"

typedef unsigned int u_int;
// --- Non-member Functions ---

// we pass this struct around instead of the individual vars
struct Params {
  int argc;
  char **argv;
  // plaintext or ciphertext
  std::string message;
  // file to output results
  std::string output_filename;
  // file to read in message from
  std::string input_filename;
  // file to store/retrieve key
  std::string key_filename;
  // number of alphabets
  u_int noa;
  // encryption or decryption?
  bool enc;
  // interactive mode?
  bool cli_exec;
  // how many threads to spawn
  u_int num_threads;
  // are we just printing the version?
  bool ver;
};

class Runner {
public:

  Runner(int &argc, char *argv[]);
  Runner() = delete;
  Runner(Runner &runner_obj) = delete;
  ~Runner();

  // we will pass around p instead of a long args signature
  // initialized with default values
  Params p{ 0, nullptr, "", "", "", "vc.key", 1, false, false, 2, false};

  // application version
  const char* VERSION = "0.2";

  // contains the main logic of the application (keeps main() smaller)
  bool run();

  // prints out the version information
  void print_version() const;

private:

  void error();
  void lower(std::string &e_or_d);
  void flush();
  // Used to display the interactive portion of the application.
  void shell();
  // lets look at what the user input
  bool parse_args();
};

#endif
